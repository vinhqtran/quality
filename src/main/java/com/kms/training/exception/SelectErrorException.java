package com.kms.training.exception;

/**
 * Created by vinhqtran on 3/19/2015.
 */
public class SelectErrorException extends FlowException {
    public SelectErrorException() {
        super();
    }

    public SelectErrorException(String arg0) {
        super(arg0);
    }

    public SelectErrorException(String message, Throwable ex) {
        super(message, ex);
    }
}
