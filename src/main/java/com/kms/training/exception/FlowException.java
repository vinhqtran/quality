package com.kms.training.exception;

/**
 *
 */
public class FlowException extends Exception {
    public FlowException() {
        super();
    }

    public FlowException(String arg0) {
        super(arg0);
    }

    public FlowException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }
}


