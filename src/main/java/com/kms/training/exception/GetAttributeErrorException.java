package com.kms.training.exception;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class GetAttributeErrorException extends FlowException {
    public GetAttributeErrorException() {
        super();
    }

    public GetAttributeErrorException(String arg0) {
        super(arg0);
    }

    public GetAttributeErrorException(String message, Throwable ex) {
        super(message, ex);
    }
}
