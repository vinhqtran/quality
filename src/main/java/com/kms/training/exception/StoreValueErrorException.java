package com.kms.training.exception;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class StoreValueErrorException extends FlowException {
    public StoreValueErrorException() {
        super();
    }

    public StoreValueErrorException(String arg0) {
        super(arg0);
    }

    public StoreValueErrorException(String message, Throwable ex) {
        super(message, ex);
    }
}
