package com.kms.training.exception;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class ToConsoleErrorException extends FlowException {
    public ToConsoleErrorException() {
        super();
    }

    public ToConsoleErrorException(String arg0) {
        super(arg0);
    }

    public ToConsoleErrorException(String message, Throwable ex) {
        super(message, ex);
    }
}
