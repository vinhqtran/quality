package com.kms.training.exception;

/**
 *
 */
public class ConnectErrorException extends FlowException {
    public ConnectErrorException() {
        super();
    }

    public ConnectErrorException(String arg0) {
        super(arg0);
    }

    public ConnectErrorException(String message, Throwable ex) {
        super(message, ex);
    }
}
