package com.kms.training.flow;

import com.kms.training.exception.GetAttributeErrorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class GetAttributeWorker extends AbstractWorker {

    private static final Log LOG = LogFactory.getLog(GetAttributeWorker.class);
    public static final String GETATTRIBUTE = "getAttribute";
    public static final String SAVETEMP = "savetemp";

    @Override
    public void set(FlowData data) {
        this.data = data;
    }

    @Override
    public FlowData get() {
        return data;
    }

    @Override
    public void execute() throws GetAttributeErrorException {
        if(!isValidInput()) {
            throw new GetAttributeErrorException("Invalid Input");
        }

        Elements selectNodes = (Elements) data.get(SelectWorker.SAVENODES);
        LOG.debug(String.format("Got selectNodes from SAVENODES", selectNodes));
        LOG.warn(String.format("Did not check type cast to Elements for selectNodes",""));

        List<String> tempvalues = new ArrayList<String>();
        String value = (String) data.get(GETATTRIBUTE);
        LOG.debug(String.format("Got attribute from FlowData", value));
        LOG.warn(String.format("Did not check type cast to String for attribute"," "));

        for (Element node : selectNodes) {
            tempvalues.add(node.attr(value));
        }

        LOG.info(String.format("Got values from selectNodes", tempvalues));

        data.put(SAVETEMP, tempvalues);
    }

    @Override
    protected boolean isValidInput() {
        boolean isNotNull = (null != data);

        return isNotNull && data.containsKey(GETATTRIBUTE);
    }
}
