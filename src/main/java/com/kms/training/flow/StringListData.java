package com.kms.training.flow;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class StringListData extends HashMap<String, List<String>> implements FlowData<List<String>> {

    public static final String DATA = "data";

    public StringListData(List<String> data) {
        set(data);
    }

    public List<String> get() {
        return this.get(DATA);
    }

    public void set(List<String> data) {
        this.put(DATA, data);
    }
}
