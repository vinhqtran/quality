package com.kms.training.flow;

import org.omg.CORBA.*;

import java.lang.Object;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class FlowDataNew implements FlowData<Object> {

    private HashMap<String, Object> newmap;

    public FlowDataNew() {
        newmap = new HashMap<String, Object>();
    }

    @Override
    public int size() {
        return newmap.size();
    }

    @Override
    public boolean isEmpty() {
        return newmap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return newmap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return newmap.containsValue(value);
    }

    @Override
    public Object get(Object key) {
        return newmap.get(key);
    }

    @Override
    public Object put(String key, Object value) {
        return newmap.put(key, value);
    }

    @Override
    public Object remove(Object key) {
        return newmap.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Object> m) {
        newmap.putAll(m);
    }

    @Override
    public void clear() {
        newmap.clear();
    }

    @Override
    public Set<String> keySet() {
        return newmap.keySet();
    }

    @Override
    public Collection<Object> values() {
        return newmap.values();
    }

    @Override
    public Set<java.util.Map.Entry<String, Object>> entrySet() {
        return newmap.entrySet();
    }

    public static FlowData<Object> createFlowData(){
        return new FlowDataNew();
    }
}