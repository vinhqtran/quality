package com.kms.training.flow;

import com.kms.training.exception.ConnectErrorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 *
 */
public class ConnectWorker extends AbstractWorker {

    private static final Log LOG = LogFactory.getLog(ConnectWorker.class);
    public static final String URL = "url";
    public static final String SAVEDOC = "saveDoc";

    Document doc = null;

    @Override
    public void set(FlowData data) {
        this.data = data;
    }

    @Override
    public FlowData get() {
        return this.data;
    }

    @Override
    public void execute() throws ConnectErrorException {
        if (!isValidInput()) {
            throw new ConnectErrorException("Invalid Input");
        } else {
            String value = (String) this.data.get(URL);
            LOG.debug(String.format("Got URL from FlowData", value));
            LOG.warn(String.format("Did not check type cast to String for value", " "));
            LOG.info(String.format("connect to URL: %s", value));

            try {
                doc = (Document) Jsoup.connect(value).get();
                LOG.warn(String.format("Did not check connection before getting the document", ""));
                LOG.info(String.format("Got document from url", doc, value));
            } catch (IOException e) {
                LOG.error(String.format("Could not retrieve the document", value), e);
                throw new ConnectErrorException("Cannot connect");
            }

            this.data.put(SAVEDOC, doc);
        }
    }

    @Override
    protected boolean isValidInput() {

        boolean isNotNull = (null != data);

        return isNotNull && data.containsKey(URL);
    }
}
