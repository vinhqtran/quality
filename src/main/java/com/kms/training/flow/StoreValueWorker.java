package com.kms.training.flow;

import com.kms.training.exception.StoreValueErrorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class StoreValueWorker extends AbstractWorker {

    private static final Log LOG = LogFactory.getLog(StoreValueWorker.class);
    public static final String STOREVALUE = "storeValue";
    public static final String SAVEVALUES = "savevalues";

    @Override
    public void set(FlowData data) {
        this.data = data;
    }

    @Override
    public FlowData get() {
        return data;
    }

    @Override
    public void execute() throws StoreValueErrorException {
        if(!isValidInput()) {
            throw new StoreValueErrorException("Invalid Input");
        }

        List<String> valuesList = new ArrayList<>();
        valuesList.addAll((Collection<? extends String>) data.get(GetAttributeWorker.SAVETEMP));
        LOG.info(String.format("Got tempt values from SAVETEMP", valuesList));
        LOG.warn(String.format("Did not check type cast to List<String> for values"," "));

        data.put(SAVEVALUES, valuesList);
    }

    @Override
    protected boolean isValidInput() {
        boolean isNotNull = (null != data);

        return isNotNull && data.containsKey(STOREVALUE);
    }
}
