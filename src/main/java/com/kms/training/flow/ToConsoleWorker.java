package com.kms.training.flow;


import com.kms.training.exception.ToConsoleErrorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vinhqtran on 3/20/2015.
 */
public class ToConsoleWorker extends AbstractWorker {

    private static final Log LOG = LogFactory.getLog(ToConsoleWorker.class);
    public static final String TOCONSOLE = "toConsole";
    public static final String VALUEFINAL = "valuefinal";

    @Override
    public void set(FlowData data) {
        this.data = data;
    }

    @Override
    public FlowData get() {
        return data;
    }

    @Override
    public void execute() throws ToConsoleErrorException {
        if(!isValidInput()) {
            throw new ToConsoleErrorException("Invalid Input");
        }

        List<String> toValueList = new ArrayList<String>();
        toValueList.addAll((Collection<? extends String>) data.get(StoreValueWorker.SAVEVALUES));
        LOG.info(String.format("Got values from SAVEVALUES", toValueList));
        LOG.warn(String.format("Did not check type cast to List<String> for values",""));

        data.put(VALUEFINAL, toValueList);
    }
    @Override
    protected boolean isValidInput() {
        boolean isNotNull = (null != data);

        return isNotNull && data.containsKey(TOCONSOLE);
    }
}
