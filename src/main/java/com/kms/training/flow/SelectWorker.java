package com.kms.training.flow;

import com.kms.training.exception.SelectErrorException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class SelectWorker extends AbstractWorker {

    private static final Log LOG = LogFactory.getLog(SelectWorker.class);
    public static final String SELECT = "select";
    public static final String SAVENODES = "savenodes";

    @Override
    public void set(FlowData data) {
        this.data = data;
    }

    @Override
    public FlowData get() {
        return data;
    }

    @Override
    public void execute() throws SelectErrorException {
        if(!isValidInput()) {
            throw new SelectErrorException("Invalid Input");
        }

        String value = (String) data.get(SELECT);
        LOG.debug(String.format("Got selector from flowData", value));
        LOG.warn(String.format("Did not check type cast to String for value"," "));

        Document doc = (Document) data.get(ConnectWorker.SAVEDOC);
        LOG.debug(String.format("Got document from SAVEDOC", doc));
        LOG.warn(String.format("Did not check type cast to document for doc"," "));

        Elements selectedNodes = doc.select(value);
        LOG.info(String.format("Got selectNodes from document", selectedNodes));
        this.data.put(SAVENODES, selectedNodes);
    }

    @Override
    protected boolean isValidInput() {
        boolean isNotNull = (null != data);

        return isNotNull && data.containsKey(SELECT);
    }
}