package com.kms.training.flow;

import com.kms.training.exception.FlowException;

/**
 * Defines a simple worker in a flow
 */
public interface Worker {
    /**
     * set input to worker
     * @param data
     */
    void set(FlowData data);

    /**
     * get output from worker
     * @return
     */
    FlowData get();


    /**
     * execute worker
     */
    void execute() throws FlowException;
}
