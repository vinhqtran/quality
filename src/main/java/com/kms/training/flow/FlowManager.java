package com.kms.training.flow;

import com.kms.training.exception.*;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 */
public class FlowManager {

    private static final Log LOG = LogFactory.getLog(FlowManager.class);

    private FlowDataNew flowData;
    private List<Worker> worker = new ArrayList<Worker>();
    private Properties prop = new Properties();

    public FlowManager(String propFileName) throws IllegalArgumentException, IOException, GetAttributeErrorException {

        InputStream inputStream = FlowManager.class.getClassLoader().getResourceAsStream(propFileName);
        LOG.debug(String.format("Got InputStream from FileName", inputStream, propFileName));
        prop = new Properties();
        flowData = new FlowDataNew();

        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        String parsing_path = prop.getProperty("script");
        LOG.info(String.format("Using parsing path %s...", parsing_path));

        if (parsing_path == null) {
            throw new IllegalArgumentException("Not property of script");
        }

        String[] split = parsing_path.trim().split("->");

        for (String script : split) {
            String[] splitElement = script.trim().split(" ");
            int operatorLength = splitElement.length;
            String instruction = null;
            String value = null;

            if (operatorLength < 1) {
                throw new IllegalArgumentException("Script must have one instruction at minimum");
            } else if (operatorLength > 2) {
                throw new IllegalArgumentException("Script must have one instruction and one value at maximum");
            } else {
                instruction = splitElement[0];
                if (operatorLength >= 2) {
                    value = splitElement[1];
                }
            }

            if (instruction.equalsIgnoreCase("connect")) {
                if ((value != null) && value.contains("val_")) {
                    String keyToValue = value.substring("val_".length());
                    String url = prop.getProperty(keyToValue);

                    if (value == null) {
                        throw new IllegalArgumentException("Not property of url");
                    }

                    flowData.put(keyToValue, url);
                    ConnectWorker connectWorker = new ConnectWorker();
                    connectWorker.set(flowData);
                    worker.add(connectWorker);
                } else {
                    throw new IllegalArgumentException("'connect' must have value format 'val_%URL'");
                }
            } else if (instruction.equalsIgnoreCase("select")) {
                if (value != null) {
                    flowData.put(instruction, value);
                    SelectWorker selectWorker = new SelectWorker();
                    selectWorker.set(flowData);
                    worker.add(selectWorker);
                } else {
                    throw new IllegalArgumentException("'select' must contain value");
                }
            } else if (instruction.equalsIgnoreCase("getAttribute")) {
                if (value != null) {
                    flowData.put(instruction, value);
                    GetAttributeWorker getAttributeWorker = new GetAttributeWorker();
                    getAttributeWorker.set(flowData);
                    worker.add(getAttributeWorker);
                } else {
                    throw new IllegalArgumentException("'getAttribute' must contain value");
                }
            } else if (instruction.equalsIgnoreCase("storeValue")) {
                    flowData.put(instruction, null);
                    StoreValueWorker storeValueWorker = new StoreValueWorker();
                    storeValueWorker.set(flowData);
                    worker.add(storeValueWorker);
            } else if (instruction.equalsIgnoreCase("toConsole")) {
                    flowData.put(instruction, null);
                    ToConsoleWorker toConsoleWorker = new ToConsoleWorker();
                    toConsoleWorker.set(flowData);
                    worker.add(toConsoleWorker);
            } else {
                throw new IllegalArgumentException("there is no such a function '" + instruction + "'");
            }
        }
    }

    public FlowManager(List<Worker> workers) {
        this.worker = workers;
    }

    public FlowManager() {
        this.worker = null;
    }

    public List<Worker> getWorkers() {
        return this.worker;
    }

    public FlowData run() throws FlowException {
            try {
                for (Worker aWorker : worker) {
                    aWorker.execute();
                }
            } catch (FlowException e) {
                LOG.error(String.format("Got FlowException when worker is executing", worker.getClass()), e);
            }

        StringListData returnData = new StringListData((List<String>) flowData.get(StoreValueWorker.SAVEVALUES));

        return returnData;
    }
}
