package com.kms.training.flow;

import java.util.Map;

public interface FlowData<V> extends Map<String, V> {

}
