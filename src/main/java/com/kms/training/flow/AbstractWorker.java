package com.kms.training.flow;

/**
 *
 */
public abstract class AbstractWorker implements Worker {

    protected FlowData data;

    protected abstract boolean isValidInput();
}
