package com.kms.training;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 */
public class Application {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        Properties prop = new Properties();
        String propFileName = "config.properties";

        InputStream inputStream = Application.class.getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        Document doc = null;

        String parsing_path = prop.getProperty("script");
        //print("Using parsing path %s...", parsing_path);

        String[] split = parsing_path.trim().split("->");

        Elements selectedNodes = null;

        List<String> tempValues = new ArrayList<String>();

        List<String> urlList = new ArrayList<String>();

        List<String> valueList = new ArrayList<String>();

        for (String script : split) {

            String[] split1 = script.trim().split(" ");

            int operatorLength = split1.length;

            if (operatorLength < 1) {
                throw new IllegalArgumentException("Script must have one instruction at minimum");
            } else if (operatorLength > 2) {
                throw new IllegalArgumentException("Script must have one instruction and one value at maximum");
            }

            String instruction = split1[0];

            String value = null;
            if (operatorLength >= 2) {
                value = split1[1];
            }

            if (instruction.equalsIgnoreCase("connect")) {
                //Reassign doc to new document
                //print("connect to URL: %s", value);

                if (value != null && value.contains("val_")) {
                    String keyToValue = value.substring("val_".length());
                    System.out.println(keyToValue);
                    value = prop.getProperty(keyToValue);
                    System.out.println(value);
                    //print("get real value for val_%s: %s", keyToValue, value);
                }
                doc = Jsoup.connect(value).get();
            }
              else if (instruction.equalsIgnoreCase("select")) {
                /*This is current node being selected.
                  This only works on current document
                 */
                System.out.println(value);
                selectedNodes = doc.select(value);
                //System.out.println(selectedNodes);
            }
            else if (instruction.equalsIgnoreCase("getAttribute")) {
                //This only works on selected nodes and outputs will be stored in tempValues
                for (Element node : selectedNodes) {
                    tempValues.add(node.attr(value));
                    //System.out.println(tempValues);
                }

            }
// else if (instruction.equalsIgnoreCase("getText")) {
//                //This only works on selected nodes and outputs will be stored in tempValues
//                for (Element node : selectedNodes) {
//                    tempValues.add(node.text());
//                }
//            }
// else if (instruction.equalsIgnoreCase("storeURL")) {
//                urlList.addAll(tempValues);                                                                                     //tempValues
//                                                                                                                                //urlList
//                //clean up tempValues                                                                                           //valueList
//                tempValues.clear();
//
//            }
            else if (instruction.equalsIgnoreCase("storeValue")) {
                valueList.addAll(tempValues);

                //clean up tempValues
                tempValues.clear();
                //System.out.println(valueList);
            }
// else if (instruction.equalsIgnoreCase("toFile")) {
//
//            } else if (instruction.equalsIgnoreCase("open")) {
//
//            }
//             else if (instruction.equalsIgnoreCase("toConsole")) {
//
//                for (String text: valueList) {
//                    print("%s", text);
//                }
//            }
        }

    }



    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

}