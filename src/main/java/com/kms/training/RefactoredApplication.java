package com.kms.training;

import com.kms.training.exception.FlowException;
import com.kms.training.flow.FlowManager;
import com.kms.training.flow.StringListData;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 */
public class RefactoredApplication {

    public static final Log LOG = LogFactory.getLog(RefactoredApplication.class);
    public static void main(String[] args) throws IOException, FlowException {

        FlowManager flowManager = new FlowManager("config.properties");
        LOG.debug(String.format("Created FlowManager", flowManager));

        final StringListData finalOutput = (StringListData) flowManager.run();
        LOG.info(String.format("Got StringListData",finalOutput));

        final List<String> list = finalOutput.get();

        for (String item: list) {
            print("%s", item);
        }
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }
}
